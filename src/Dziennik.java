public class Dziennik {

    private String[] students;
    private int studentsNumber = 0;

    public Dziennik() {
        students = new String[5];
    }

    public void addStudent(String name) {
        students[studentsNumber] = name;
        studentsNumber++;
    }

    public void removeStudent() {
        students[studentsNumber] = null;
    }

    public void printDziennik() {
        for (String name: students) {
            System.out.println(name);
        }
    }
}

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Dziennik dziennik = new Dziennik();

        System.out.print("Write student name: ");
        String name = scanner.nextLine();
        dziennik.addStudent(name);
        dziennik.addStudent(name);
        dziennik.addStudent(name);
        dziennik.addStudent(name);
        dziennik.removeStudent();
        dziennik.printDziennik();
    }
}
